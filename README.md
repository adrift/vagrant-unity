# vagrant-unity

Vagrant project to bring up a Windows 10-based Unity development environment (or build server!)

## usage

1. Install [Vagrant](https://www.vagrantup.com/downloads.html)
2. Install [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
3. Clone this repository
4. `cd vagrant-unity`
5. `vagrant up`

Completion of this script will produce a virtual machine running in Virtualbox that is Unity-ready