param (
  [String]
  $unity_version = '5.6.7f1',

  [String]
  $unity_download_url = "https://download.unity3d.com/download_unity/e80cc3114ac1/Windows64EditorInstaller/UnitySetup64-$unity_version.exe",

  [String]
  $workdir = 'C:\tmp',

  [String]
  $unity_installer_path = "$workdir\UnitySetup64-$unity_version.exe",

  [String]
  $expected_hash = '918FCD42A2C8EB7984972508E1D8DAB693F9E7721CAE7642EC60AA4E99C72D12'
)

if (-Not (Test-Path $workdir)) {
  Write-Output "Creating $workdir"
  New-Item -ItemType Directory -Path $workdir
}

if (-Not (Test-Path $unity_installer_path)) { 
  Write-Output "Downloading $unity_download_url to $unity_installer_path"
  Invoke-WebRequest $unity_download_url -OutFile $unity_installer_path
}

$detected_hash = (Get-FileHash -Algorithm SHA256 -Path $unity_installer_path).Hash

try {
  Write-Host -NoNewline "Checksum verify..."
  $detected_hash -eq $expected_hash
}
catch { 
  throw "ERROR: File integrity check failed. Expected $expected_hash, but got $detected_hash"
}

Write-Output "Installing Unity, by default version $unity_version"
& $unity_installer_path /S
